#include "mainwindow.h"
#include "ui_mainwindow.h"

#include<iostream>
#include<string>
#include <QMessageBox>
#include <ctime>

using namespace std;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

bool isPrime( long int prime)
{
    long int i, j;

    j = (long int)sqrt((long double)prime);

    for ( i = 2; i <= j; i++)
    {
        if ( prime % i == 0 )
        {
            return false;
        }
    }

    return true;
}

long int greatestCommonDivisor( long int e, long int t )
{
    while ( e > 0 )
    {
        long int myTemp;

        myTemp = e;
        e = t % e;
        t = myTemp;
    }

    return t;
}

long int calculateE( long int t )
{
    // Выбирается целое число e ( 1 < e < t ) // взаимно простое со значением функции Эйлера (t)

    long int e;

    for ( e = 2; e < t; e++ )
    {
        if (greatestCommonDivisor( e, t ) == 1 )
        {
            return e;
        }
    }

    return -1;
}


long int calculateD( long int e, long int t)
{
    // Вычисляется число d, мультипликативно обратное к числу e по модулю φ(n), то есть число, удовлетворяющее сравнению:
    //    d ⋅ e ≡ 1 (mod φ(n))

    long int d;
    long int k = 1;

    while ( 1 )
    {
        k = k + t;

        if ( k % e == 0)
        {
            d = (k / e);
            return d;
        }
    }

}


long int encrypt( long int i, long int e, long int n )
{
    long int current, result;

    current = i - 97;
    result = 1;

    for ( long int j = 0; j < e; j++ )
    {
        result = result * current;
        result = result % n;
    }

    return result;
}

long int decrypt(long int i, long int d, long int n)
{
    long int current, result;

    current = i;
    result = 1;

    for ( long int j = 0; j < d; j++ )
    {
        result = result * current;
        result = result % n;
    }

    return result + 97;
}

void MainWindow::on_pushButton_2_clicked()
{
    std::string msg;
    msg=ui->textEdit->toPlainText().trimmed().toStdString();
    long int p, q, n, t, e, d;

    long int encryptedText[100];
    memset(encryptedText, 0, sizeof(encryptedText));

    long int decryptedText[100];
    memset(decryptedText, 0, sizeof(decryptedText));

       bool flag;

       // Cоздание открытого и секретного ключей

       // 1. Выбираются два различных случайных простых числа p и q заданного размера
       p=5;
       q=7;
       // 2. Вычисляется их произведение n = p ⋅ q, которое называется модулем.
       n = p * q;
       // 3. Вычисляется значение функции Эйлера от числа n: φ(n) = (p−1)⋅(q−1)
       t = ( p - 1 ) * ( q - 1 );
       std::cout << "Result of computing Euler's totient function:\t t = " << t << std::endl;

       // 4. Выбирается целое число e ( 1 < e < φ(n) ), взаимно простое со значением функции Эйлера (t)
       //	  Число e называется открытой экспонентой
       e = calculateE( t );

       // 5. Вычисляется число d, мультипликативно обратное к числу e по модулю φ(n), то есть число, удовлетворяющее сравнению:
       //    d ⋅ e ≡ 1 (mod φ(n))
       d = calculateD( e, t );

       // 6. Пара {e, n} публикуется в качестве открытого ключа RSA
       QString N,E;
       N=QString::number(n);
       E=QString::number(e);
       ui->textBrowser_3->setText("n = "+N+" "+"e = "+E);




       // encryption

       for (long int i = 0; i < msg.length(); i++)
       {
           encryptedText[i] = encrypt( msg[i], e, n);
       }

       std::cout << "\nTHE ENCRYPTED MESSAGE IS:" << std::endl;
       QString Enc="";

       for ( long int i = 0; i < msg.length(); i++ )
       {
           Enc+= QString((char)encryptedText[i]);
       }


       //decryption

       for (long int i = 0; i < msg.length(); i++)
       {
           decryptedText[i] = decrypt(encryptedText[i], d, n);
       }

       QString Dec="";

       for (long int i = 0; i < msg.length(); i++)
       {
           Dec+= QString((char)decryptedText[i]);
       }


    ui->textBrowser->setText(Dec);
    ui->textBrowser_2->setText(Enc);
}


void MainWindow::on_pushButton_3_clicked()
{
    if (QMessageBox::question(this, "Exit", "Are you sure?", QMessageBox::Yes | QMessageBox::No)==QMessageBox::Yes)
        this->close();
}

void MainWindow::on_pushButton_clicked()
{
    if (QMessageBox::question(this, "Exit", "Are you sure?", QMessageBox::Yes | QMessageBox::No)==QMessageBox::Yes)
        this->close();
}
